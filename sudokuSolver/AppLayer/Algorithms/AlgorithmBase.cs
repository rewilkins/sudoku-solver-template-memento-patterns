﻿using AppLayer.Puzzle;
using System.Collections.Generic;

namespace AppLayer.Algorithms
{
    public abstract class AlgorithmBase
    { 
        
        public struct Point { public int row; public int col; }

        public Sudoku sudoku;
        private Queue<List<Point>> markedCells = new Queue<List<Point>>();
        public bool boardModified;
        public GridScanner gridScanner = new GridScanner();
        public List<int[]> modifyingCells = new List<int[]>();
        public List<int[]> modedCells = new List<int[]>();

        public List<Point> getNextMarkedCell()
        {
            if (markedCells.Count > 0) return markedCells.Dequeue();
            else return null;
        }

        public void markCell(List<Point> cell)
        {
            markedCells.Enqueue(cell);
        }

        public void clearMarkedCells()
        {
            markedCells.Clear();
        }

        public bool execute()
        {
            gridScanner.setTargetSudoku(sudoku);
            findCells();
            return update();
        }

        protected abstract void findCells();

        protected abstract bool update();
    }
}
