﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;

namespace AppLayer.Algorithms
{
    public class BruteForce : AlgorithmBase
    {

        private string targetPotential;

        override
        protected void findCells()
        {

            clearMarkedCells();

            for (int i = 2; i < sudoku.getSize(); i++) // looking for cells with this number of potentials in the cell
            {
                for (int row = 0; row < sudoku.getSize(); row++)
                {
                    for (int col = 0; col < sudoku.getSize(); col++)
                    {
                        if (sudoku.grid.getGrid()[row, col].potentialValues.Count == i)
                        {
                            targetPotential = sudoku.grid.getGrid()[row, col].potentialValues[0];
                            markCell(new List<Point> { new Point { row = row, col = col } });
                            return;
                        }
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modifyingCells.Clear();
            modedCells.Clear();
            List<Point> cell = getNextMarkedCell();
            boardModified = false;
            while (cell != null && sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Count > 0)
            {
                boardModified = true;

                sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Remove(targetPotential);
                CareTaker.getInstance().storeGridToStack(sudoku.grid.storeInMomento());
                sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Clear();
                sudoku.grid.getGrid()[cell[0].row, cell[0].col].value = targetPotential;
                modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });

                //update rows
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (col != cell[0].col)
                    {
                        if (cell[0].row == 14 && col == 8)
                        {
                            Console.Write(""); // debug trap.  Change row && col and place brake point here.
                        }
                        bool removed;
                        removed = sudoku.grid.getGrid()[cell[0].row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                        if (removed) modedCells.Add(new int[2] { cell[0].row, col });
                    }
                }

                // update cols
                for (int row = 0; row < sudoku.getSize(); row++)
                {
                    if (row != cell[0].row)
                    {
                        if (row == 14 && cell[0].col == 8)
                        {
                            Console.Write(""); // debug trap.  Change row && col and place brake point here.
                        }
                        bool removed;
                        removed = sudoku.grid.getGrid()[row, cell[0].col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                        if (removed) modedCells.Add(new int[2] { row, cell[0].col });
                    }
                }

                // update block
                int[] block = sudoku.getBlockLocs()[sudoku.grid.getGrid()[cell[0].row, cell[0].col].blockID];
                for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++)
                {
                    for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                    {
                        if (row != cell[0].row && col != cell[0].col)
                        {
                            if (row == 14 && col == 8)
                            {
                                Console.Write(""); // debug trap.  Change row && col and place brake point here.
                            }
                            bool removed;
                            removed = sudoku.grid.getGrid()[row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                            if (removed) modedCells.Add(new int[2] { row, col });
                        }
                    }
                }

                cell = getNextMarkedCell();
            }
            return boardModified;
        }
    }
}
