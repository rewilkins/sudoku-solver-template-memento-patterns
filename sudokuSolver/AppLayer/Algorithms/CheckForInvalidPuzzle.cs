﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Algorithms
{
    public class CheckForInvalidPuzzle : AlgorithmBase
    {

        List<Point> emptyCells = new List<Point>();

        override
        protected void findCells()
        {

            clearMarkedCells();

            // check to make sure that there is no cell with no solution and no possibilities remaining
            for (int row = 0; row < sudoku.getSize(); row++)
            {
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (sudoku.grid.getGrid()[row, col].value == "-" && sudoku.grid.getGrid()[row, col].potentialValues.Count() == 0)
                    {
                        emptyCells.Add(new Point { row = row, col = col });
                        markCell(new List<Point> { new Point { row = row, col = col } });
                        return;
                    }
                }
            }

            for (int row = 0; row < sudoku.getSize(); row++)
            {
                for (int col = 0; col < sudoku.getSize(); col++)
                {

                    if (row == 13 && col == 9)
                    {
                        Console.Write(""); // debug trap.  Change row && col and place brake point here.
                    }

                    // check to see if there are two same numbers in same row, col and/or block

                    if (sudoku.grid.getGrid()[row, col].value != "-")
                    {

                        // check row
                        for (int col2 = 0; col2 < sudoku.getSize(); col2++)
                        {
                            if (col2 != col)
                            {
                                if (sudoku.grid.getGrid()[row, col].value == sudoku.grid.getGrid()[row, col2].value)
                                {
                                    markCell(new List<Point> { new Point { row = row, col = col } });
                                    markCell(new List<Point> { new Point { row = row, col = col2 } });
                                    return;
                                }
                            }
                        }

                        // check col
                        for (int row2 = 0; row2 < sudoku.getSize(); row2++)
                        {
                            if (row2 != row)
                            {
                                if (sudoku.grid.getGrid()[row, col].value == sudoku.grid.getGrid()[row2, col].value)
                                {
                                    markCell(new List<Point> { new Point { row = row, col = col } });
                                    markCell(new List<Point> { new Point { row = row2, col = col } });
                                    return;
                                }
                            }
                        }

                        // check block
                        int rowStart = sudoku.getBlockLocs()[sudoku.grid.getGrid()[row, col].blockID][0];
                        int colStart = sudoku.getBlockLocs()[sudoku.grid.getGrid()[row, col].blockID][1];
                        for (int row2 = rowStart; row2 < rowStart + Math.Sqrt(sudoku.getSize()); row2++)
                        {
                            for (int col2 = colStart; col2 < colStart + Math.Sqrt(sudoku.getSize()); col2++)
                            {
                                if (row2 != row && col2 != col)
                                {
                                    if (sudoku.grid.getGrid()[row, col].value == sudoku.grid.getGrid()[row2, col2].value)
                                    {
                                        markCell(new List<Point> { new Point { row = row, col = col } });
                                        markCell(new List<Point> { new Point { row = row2, col = col2 } });
                                        return;
                                    }
                                }
                            }
                        }

                    }

                    // flag the puzzle as invalid is there is only one remaining possibility in
                    // multiple cells in the same row, col, or block.
                    if (sudoku.grid.getGrid()[row, col].value == "-")
                    {
                        if (sudoku.grid.getGrid()[row, col].potentialValues.Count == 1)
                        {
                            // check row
                            for (int col2 = 0; col2 < sudoku.getSize(); col2++)
                            {
                                if(col2 != col)
                                {
                                    if(sudoku.grid.getGrid()[row, col2].potentialValues.Count == 1)
                                    {
                                        if (sudoku.grid.getGrid()[row, col].potentialValues[0] == sudoku.grid.getGrid()[row, col2].potentialValues[0])
                                        {
                                            markCell(new List<Point> { new Point { row = row, col = col } });
                                            markCell(new List<Point> { new Point { row = row, col = col2 } });
                                            return;
                                        }
                                    }
                                }
                            }

                            // check col
                            for (int row2 = 0; row2 < sudoku.getSize(); row2++)
                            {
                                if (row2 != row)
                                {
                                    if (sudoku.grid.getGrid()[row2, col].potentialValues.Count == 1)
                                    {
                                        if (sudoku.grid.getGrid()[row, col].potentialValues[0] == sudoku.grid.getGrid()[row2, col].potentialValues[0])
                                        {
                                            markCell(new List<Point> { new Point { row = row, col = col } });
                                            markCell(new List<Point> { new Point { row = row2, col = col } });
                                            return;
                                        }
                                    }
                                }
                            }

                            // check block
                            int rowStart = sudoku.getBlockLocs()[sudoku.grid.getGrid()[row, col].blockID][0];
                            int colStart = sudoku.getBlockLocs()[sudoku.grid.getGrid()[row, col].blockID][1];
                            for (int row2 = rowStart; row2 < rowStart + Math.Sqrt(sudoku.getSize()); row2++)
                            {
                                for (int col2 = colStart; col2 < colStart + Math.Sqrt(sudoku.getSize()); col2++)
                                {
                                    if (row2 != row && col2 != col)
                                    {
                                        if (sudoku.grid.getGrid()[row2, col2].potentialValues.Count == 1)
                                        {
                                            if (sudoku.grid.getGrid()[row, col].potentialValues[0] == sudoku.grid.getGrid()[row2, col2].potentialValues[0])
                                            {
                                                markCell(new List<Point> { new Point { row = row, col = col } });
                                                markCell(new List<Point> { new Point { row = row2, col = col2 } });
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modifyingCells.Clear();
            modedCells.Clear();
            List<Point> cell = getNextMarkedCell();
            boardModified = false;
            while (cell != null)
            {
                bool found = false;
                foreach (Point point in emptyCells)
                {
                    if (point.row == cell[0].row && point.col == cell[0].col) found = true;
                }
                if (found) sudoku.grid.getGrid()[cell[0].row, cell[0].col].value = "*";
                modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });
                boardModified = true;
                sudoku.setPuzzleInvalid();
                cell = getNextMarkedCell();
            }
            return boardModified;
        }
    }
}
