﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Algorithms
{
    public class FindAllPossibilities : AlgorithmBase
    {

        override
        protected void findCells()
        {

            clearMarkedCells();

            for (int row = 0; row < sudoku.getSize(); row++)
            {
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (sudoku.grid.getGrid()[row, col].value == "-")
                    {
                        markCell(new List<Point> { new Point { row = row, col = col } } );
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modedCells.Clear();
            List<Point> cell = getNextMarkedCell();
            boardModified = false;
            while (cell != null)
            {
                boardModified = true;
                modedCells.Add(new int[2] { cell[0].row, cell[0].col });

                sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Clear();
                foreach (string val in sudoku.getSymbols())
                {
                    if (
                        gridScanner.scanRowForValue(cell[0].row, val) ||
                        gridScanner.scanColForValue(cell[0].col, val) ||
                        gridScanner.scanBlockForValue(sudoku.grid.getGrid()[cell[0].row, cell[0].col].blockID, val)
                    ) { }
                    else
                    {
                        sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Add(val);
                    }
                }
                cell = getNextMarkedCell();
            }
            return boardModified;
        }

    }
}
