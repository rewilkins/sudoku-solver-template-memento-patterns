﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Algorithms
{
    public class GridScanner
    {

        private Sudoku targetSudoku;
        private int[] skipCell = new int[2];

        public void setTargetSudoku(Sudoku newTarget)
        {
            if (newTarget.isPuzzleValid()) targetSudoku = newTarget;
        }

        public void setSkippedCell(int row, int col)
        {
            skipCell[0] = row;
            skipCell[1] = col;
        }

        public bool scanRowForValue(int row, string targetValue)
        {
            if (targetSudoku != null)
            {
                for (int col = 0; col < targetSudoku.getSize(); col++)
                {
                    if (targetSudoku.grid.getGrid()[row, col].value == targetValue)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool scanColForValue(int col, string targetValue)
        {
            if (targetSudoku != null)
            {
                for (int row = 0; row < targetSudoku.getSize(); row++)
                {
                    if (targetSudoku.grid.getGrid()[row, col].value == targetValue)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool scanBlockForValue(int blockNum, string targetValue)
        {
            if (targetSudoku != null)
            {
                List<int[]> block = targetSudoku.getBlockLocs();
                for (int row = block[blockNum][0]; row < block[blockNum][0] + Math.Sqrt(targetSudoku.getSize()); row++)
                {
                    for (int col = block[blockNum][1]; col < block[blockNum][1] + Math.Sqrt(targetSudoku.getSize()); col++)
                    {
                        if (targetSudoku.grid.getGrid()[row, col].value == targetValue) return true;
                    }
                }
            }
            return false;
        }

        

        

        public bool scanRowPotentials(int row, string targetValue, bool skipCurCell)
        {
            if (targetSudoku != null)
            {
                for (int col = 0; col < targetSudoku.getSize(); col++)
                {
                    if (skipCurCell && row == skipCell[0] && col == skipCell[1]) { }
                    else{
                        foreach (string val in targetSudoku.grid.getGrid()[row, col].potentialValues)
                        {
                            if (val == targetValue)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool scanColPotentials(int col, string targetValue, bool skipCurCell)
        {
            if (targetSudoku != null)
            {
                for (int row = 0; row < targetSudoku.getSize(); row++)
                {
                    if (skipCurCell && row == skipCell[0] && col == skipCell[1]) { }
                    else
                    {
                        foreach (string val in targetSudoku.grid.getGrid()[row, col].potentialValues)
                        {
                            if (val == targetValue)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool scanBlockPotentials(int blockNum, string targetValue, bool skipCurCell)
        {
            if (targetSudoku != null)
            {
                List<int[]> block = targetSudoku.getBlockLocs();
                for (int row = block[blockNum][0]; row < block[blockNum][0] + Math.Sqrt(targetSudoku.getSize()); row++)
                {
                    for (int col = block[blockNum][1]; col < block[blockNum][1] + Math.Sqrt(targetSudoku.getSize()); col++)
                    {
                        if (skipCurCell && row == skipCell[0] && col == skipCell[1]) { }
                        else
                        {
                            foreach (string val in targetSudoku.grid.getGrid()[row, col].potentialValues)
                            {
                                if (val == targetValue)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public bool scanBlockRowPotentials(int block, int row, string targetValue, bool skipCurCell)
        {
            if (targetSudoku != null)
            {
                for (int col = 0; col < Math.Sqrt(targetSudoku.getSize()); col++)
                {
                    if (skipCurCell && row == skipCell[0] && col == skipCell[1]) { }
                    else
                    {
                        if (targetSudoku.grid.getGrid()[row, col].value == targetValue)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public bool scanBlockColPotentials(int block, int col, string targetValue, bool skipCurCell)
        {
            if (targetSudoku != null)
            {
                for (int row = 0; row < Math.Sqrt(targetSudoku.getSize()); row++)
                {
                    if (skipCurCell && row == skipCell[0] && col == skipCell[1]) { }
                    else
                    {
                        if (targetSudoku.grid.getGrid()[row, col].value == targetValue)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }





        public bool scanGridForBlanks()
        {
            if (targetSudoku != null)
            {
                for (int row = 0; row < targetSudoku.getSize(); row++)
                {
                    for (int col = 0; col < targetSudoku.getSize(); col++)
                    {
                        if (targetSudoku.grid.getGrid()[row, col].value == "-" && targetSudoku.grid.getGrid()[row, col].potentialValues.Count() == 0)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

    }
}
