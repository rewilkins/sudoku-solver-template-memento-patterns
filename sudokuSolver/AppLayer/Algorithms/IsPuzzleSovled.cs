﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Algorithms
{
    public class IsPuzzleSovled : AlgorithmBase
    {

        override
        protected void findCells()
        {

            clearMarkedCells();

            for (int row = 0; row < sudoku.getSize(); row++)
            {
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (sudoku.grid.getGrid()[row, col].value == "-")
                    {
                        sudoku.puzzleSolved = false;
                        return;
                    }
                }
            }
            sudoku.puzzleSolved = true;
        }

        override
        protected bool update()
        {
            return sudoku.puzzleSolved;  // locks the algorithms here is the puzzle is solved
        }

    }
}
