﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;

namespace AppLayer.Algorithms
{
    public class NakedTwin : AlgorithmBase
    {
        override
        protected void findCells()
        {

            clearMarkedCells();

            List<string[]> possiblePairs = new List<string[]>();

            // get every possible pair from the entire puzzle
            for (int i = 0; i < sudoku.getSize(); i++)
            {
                for (int j = i + 1; j < sudoku.getSize(); j++)
                {
                    string[] pair = new string[2];
                    pair[0] = sudoku.getSymbols()[i];
                    pair[1] = sudoku.getSymbols()[j];
                    possiblePairs.Add(pair);
                }
            }

            foreach (string[] pair in possiblePairs) // check one pair combo at a time and call update then move to next pair
            {
                foreach (int[] block in sudoku.getBlockLocs())
                {

                    // count the number of accurances for the current pair
                    int count = 0;
                    for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                    {
                        for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++)
                        {
                            if (sudoku.grid.getGrid()[row, col].potentialValues.Count == 2)
                            {
                                if (sudoku.grid.getGrid()[row, col].potentialValues.Contains(pair[0]) &&
                                    sudoku.grid.getGrid()[row, col].potentialValues.Contains(pair[1])
                                  )
                                {
                                    count++;
                                }
                            }
                        }
                    }

                    if (count == 2) // twins only works if there is 2 sets of the pair
                    {
                        for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                        {
                            for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++)
                            {
                                if (sudoku.grid.getGrid()[row, col].potentialValues.Count == 2) // only naked twins not hiddend twins
                                {
                                    if (sudoku.grid.getGrid()[row, col].potentialValues.Contains(pair[0]) &&
                                        sudoku.grid.getGrid()[row, col].potentialValues.Contains(pair[1])
                                      ) // found target possible twin
                                    {
                                        // check row for twin
                                        for (int col2 = block[1]; col2 < block[1] + Math.Sqrt(sudoku.getSize()); col2++)
                                        {
                                            if (col2 != col) // skip the cell we're trying to find a match for
                                            {
                                                if (sudoku.grid.getGrid()[row, col2].potentialValues.Count == 2)
                                                {
                                                    if (sudoku.grid.getGrid()[row, col2].potentialValues.Contains(pair[0]) &&
                                                        sudoku.grid.getGrid()[row, col2].potentialValues.Contains(pair[1])
                                                        )
                                                    {
                                                        markCell(new List<Point> { new Point { row = row, col = col }, new Point { row = row, col = col2 } });
                                                    }
                                                }
                                            }
                                        }
                                        // check col for twin
                                        for (int row2 = block[0]; row2 < block[0] + Math.Sqrt(sudoku.getSize()); row2++)
                                        {
                                            if (row2 != row) // skip the cell we're trying to find a match for
                                            {
                                                if (sudoku.grid.getGrid()[row2, col].potentialValues.Count == 2)
                                                {
                                                    if (sudoku.grid.getGrid()[row2, col].potentialValues.Contains(pair[0]) &&
                                                        sudoku.grid.getGrid()[row2, col].potentialValues.Contains(pair[1])
                                                       )
                                                    {
                                                        markCell(new List<Point> { new Point { row = row, col = col }, new Point { row = row2, col = col } });
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modifyingCells.Clear();
            modedCells.Clear();
            List<Point> cell = getNextMarkedCell();
            boardModified = false;
            while (cell != null && sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Count > 0)
            {
                bool added = false;  // tracks if the cell has been added to the modifyingCells list or not
                if (cell[0].row == cell[1].row)
                {
                    //update rows
                    for (int col = 0; col < sudoku.getSize(); col++)
                    {
                        if(col != cell[0].col && col != cell[1].col)
                        {
                            bool removed1 = false;
                            bool removed2 = false;
                            if (sudoku.grid.getGrid()[cell[0].row, col].potentialValues.Count > 0)
                            {
                                removed1 = sudoku.grid.getGrid()[cell[0].row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[0]);
                                removed2 = sudoku.grid.getGrid()[cell[0].row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[1]);
                            }
                            if (removed1 || removed2)
                            {
                                boardModified = true;
                                if (!added)
                                {
                                    modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });
                                    modifyingCells.Add(new int[2] { cell[1].row, cell[1].col });
                                    added = true;
                                }
                                modedCells.Add(new int[2] { cell[0].row, col });
                            }
                        }
                    }
                }
                if(cell[0].col == cell[1].col)
                {
                    // update cols
                    for (int row = 0; row < sudoku.getSize(); row++)
                    {
                        if (row != cell[0].row && row != cell[1].row)
                        {
                            bool removed1 = false;
                            bool removed2 = false;
                            if (sudoku.grid.getGrid()[row, cell[0].col].potentialValues.Count > 0){
                                removed1 = sudoku.grid.getGrid()[row, cell[0].col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[0]);
                                removed2 = sudoku.grid.getGrid()[row, cell[0].col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[1]);
                            }
                            if (removed1 || removed2)
                            {
                                boardModified = true;
                                if (!added)
                                {
                                    modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });
                                    modifyingCells.Add(new int[2] { cell[1].row, cell[1].col });
                                    added = true;
                                }
                                modedCells.Add(new int[2] { row, cell[0].col });
                            }
                        }
                    }
                }
                // update block
                int[] block = sudoku.getBlockLocs()[sudoku.grid.getGrid()[cell[0].row, cell[0].col].blockID];
                for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++)
                {
                    for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                    {
                        if (row != cell[0].row && col != cell[0].col ||
                            row != cell[1].row && col != cell[1].col)
                        {
                            bool removed1 = false;
                            bool removed2 = false;
                            if (sudoku.grid.getGrid()[row, col].potentialValues.Count > 0)
                            {
                                removed1 = sudoku.grid.getGrid()[row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[0]);
                                removed2 = sudoku.grid.getGrid()[row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[1]);
                            }
                            if (removed1 || removed2)
                            {
                                boardModified = true;
                                if (!added)
                                {
                                    modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });
                                    modifyingCells.Add(new int[2] { cell[1].row, cell[1].col });
                                    added = true;
                                }
                                modedCells.Add(new int[2] { row, col });
                            }
                        }
                    }
                }
                cell = getNextMarkedCell();
            }
            return boardModified;
        }

    }
}
