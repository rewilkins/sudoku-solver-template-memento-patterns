﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;

namespace AppLayer.Algorithms
{
    public class NumRequiredInSpot : AlgorithmBase
    {
        override
        protected void findCells()
        {

            clearMarkedCells();

            boardModified = false;
            for (int row = 0; row < sudoku.getSize(); row++)
            {
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (sudoku.grid.getGrid()[row, col].value == "-")
                    {

                        if (row == 7 && col == 7)
                        {
                            Console.Write(""); // debug trap.  Change row && col and place brake point here.
                        }

                        gridScanner.setSkippedCell(row, col);
                        foreach (string val in sudoku.grid.getGrid()[row, col].potentialValues)
                        {
                            if(
                                !gridScanner.scanColPotentials(col, val, true) ||
                                !gridScanner.scanRowPotentials(row, val, true) ||
                                !gridScanner.scanBlockPotentials(sudoku.grid.getGrid()[row, col].blockID, val, true)
                            )
                            {
                                sudoku.grid.getGrid()[row, col].potentialValues.Clear();
                                sudoku.grid.getGrid()[row, col].potentialValues.Add(val);
                                markCell(new List<Point> { new Point { row = row, col = col } });
                                break;
                            }
                        }
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modifyingCells.Clear();
            modedCells.Clear();
            List<Point> cell = getNextMarkedCell();
            boardModified = false;
            while (cell != null && sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Count > 0)
            {
                boardModified = true;
                modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });

                sudoku.grid.getGrid()[cell[0].row, cell[0].col].value = sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[0];
                sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Clear();

                //update rows
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (col != cell[0].col)
                    {
                        if (cell[0].row == 14 && col == 8)
                        {
                            Console.Write(""); // debug trap.  Change row && col and place brake point here.
                        }
                        bool removed;
                        removed = sudoku.grid.getGrid()[cell[0].row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                        if (removed) modedCells.Add(new int[2] { cell[0].row, col });
                    }
                }

                // update cols
                for (int row = 0; row < sudoku.getSize(); row++)
                {
                    if (row != cell[0].row)
                    {
                        if (row == 14 && cell[0].col == 8)
                        {
                            Console.Write(""); // debug trap.  Change row && col and place brake point here.
                        }
                        bool removed;
                        removed = sudoku.grid.getGrid()[row, cell[0].col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                        if (removed) modedCells.Add(new int[2] { row, cell[0].col });
                    }
                }

                // update block
                int[] block = sudoku.getBlockLocs()[sudoku.grid.getGrid()[cell[0].row, cell[0].col].blockID];
                for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++)
                {
                    for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                    {
                        if (row != cell[0].row && col != cell[0].col)
                        {
                            if (row == 14 && col == 8)
                            {
                                Console.Write(""); // debug trap.  Change row && col and place brake point here.
                            }
                            bool removed;
                            removed = sudoku.grid.getGrid()[row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                            if (removed) modedCells.Add(new int[2] { row, col });
                        }
                    }
                }

                cell = getNextMarkedCell();
            }
            return boardModified;
        }
    }
}
