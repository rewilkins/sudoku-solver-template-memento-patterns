﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;

namespace AppLayer.Algorithms
{
    public class OnlyOnePossibility : AlgorithmBase
    {

        override
        protected void findCells()
        {

            clearMarkedCells();

            boardModified = false;
            for (int row = 0; row < sudoku.getSize(); row++)
            {
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    List<string> list = sudoku.grid.getGrid()[row, col].potentialValues;
                    int count = list.Count;
                    if (count == 1)
                    {
                        markCell(new List<Point> { new Point { row = row, col = col } });
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modifyingCells.Clear();
            modedCells.Clear();
            List<Point> cell = getNextMarkedCell();
            boardModified = false;
            while (cell != null && sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Count > 0)
            {
                boardModified = true;
                modifyingCells.Add(new int[2] { cell[0].row, cell[0].col });

                sudoku.grid.getGrid()[cell[0].row, cell[0].col].value = sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues[0];
                sudoku.grid.getGrid()[cell[0].row, cell[0].col].potentialValues.Clear();

                //update rows
                for (int col = 0; col < sudoku.getSize(); col++)
                {
                    if (col != cell[0].col)
                    {
                        bool removed;
                        removed = sudoku.grid.getGrid()[cell[0].row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                        if (removed) modedCells.Add(new int[2] { cell[0].row, col });
                    }
                }

                // update cols
                for (int row = 0; row < sudoku.getSize(); row++)
                {
                    if (row != cell[0].row)
                    {
                        bool removed;
                        removed = sudoku.grid.getGrid()[row, cell[0].col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                        if (removed) modedCells.Add(new int[2] { row, cell[0].col });
                    }
                }

                // update block
                int[] block = sudoku.getBlockLocs()[sudoku.grid.getGrid()[cell[0].row, cell[0].col].blockID];
                for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++)
                {
                    for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                    {
                        if (row != cell[0].row && col != cell[0].col)
                        {
                            bool removed;
                            removed = sudoku.grid.getGrid()[row, col].potentialValues.Remove(sudoku.grid.getGrid()[cell[0].row, cell[0].col].value);
                            if (removed) modedCells.Add(new int[2] { row, col });
                        }
                    }
                }

                cell = getNextMarkedCell();
            }
            return boardModified;
        }

    }
}
