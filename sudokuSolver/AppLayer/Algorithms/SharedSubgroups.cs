﻿using AppLayer.Puzzle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Algorithms
{
    public class SharedSubgroups : AlgorithmBase
    {

        List<string> targetPotentials = new List<string>();

        override
        protected void findCells()
        {

            List<Point> possibleMarkableCells = new List<Point>();

            clearMarkedCells();

            targetPotentials.Clear();
            // scan each block
            foreach (int[] block in sudoku.getBlockLocs())
            {
                // scan each cell in each block
                for (int row = block[0]; row < block[0] + Math.Sqrt(sudoku.getSize()); row++)
                {
                    for (int col = block[1]; col < block[1] + Math.Sqrt(sudoku.getSize()); col++) 
                    {

                        if (row == 4 && col == 0)
                        {
                            Console.Write(""); // debug trap.  Change row && col and place brake point here.
                        }

                        // scan each potential in the current cell
                        foreach (string potential in sudoku.grid.getGrid()[row, col].potentialValues)
                        {

                            if (potential == "1")
                            {
                                Console.Write(""); // debug trap.  Change potential and place brake point here.
                            }

                            bool found1 = false;
                            bool found2 = false;

                            // scan the block's row for matching potentials
                            possibleMarkableCells.Clear();
                            for (int col2 = block[1]; col2 < block[1] + Math.Sqrt(sudoku.getSize()); col2++)
                            {
                                if (col2 != col) // skip cur cell
                                {
                                    foreach (string potential2 in sudoku.grid.getGrid()[row, col2].potentialValues)
                                    {
                                        if (potential2 == potential) // if found a matching potential
                                        {
                                            found1 = true; // allow us to proceed to check the grids row for matching potentials
                                            possibleMarkableCells.Add(new Point { row = row, col = col2 }); // track this matching cell for if this find actually modifies the board
                                        }
                                    }
                                }
                            }

                            if (found1)
                            {

                                // search the grid's row for matching potentials, skip the current block
                                for (int col2 = 0; col2 < sudoku.getSize(); col2++)
                                {
                                    if (sudoku.grid.getGrid()[row, col2].blockID != sudoku.grid.getGrid()[row, col].blockID) // skip cur block
                                    {
                                        foreach (string potential2 in sudoku.grid.getGrid()[row, col2].potentialValues)
                                        {
                                            if (potential2 == potential) // if found a matching potential
                                            {
                                                found2 = true; // allow us to proceed to check the grids row for matching potentials
                                                break;
                                            }
                                        }
                                    }
                                    if (found2) break;
                                }


                                if (!found2) // if there is no other potential in the column then...
                                {
                                    List<Point> temp = new List<Point>();
                                    temp.Clear();
                                    temp.Add(new Point { row = row, col = col });
                                    foreach (Point point in possibleMarkableCells)
                                    {
                                        temp.Add(point);
                                    }
                                    markCell(temp);
                                    targetPotentials.Add(potential);
                                }
                            }

                            found1 = false;
                            found2 = false;

                            // scan the block's col for matching potentials
                            possibleMarkableCells.Clear();
                            for (int row2 = block[0]; row2 < block[0] + Math.Sqrt(sudoku.getSize()); row2++)
                            {
                                if (row2 != row) // skip cur cell
                                {
                                    foreach (string potential2 in sudoku.grid.getGrid()[row2, col].potentialValues)
                                    {
                                        if (potential2 == potential) // if found a matching potential
                                        {
                                            found1 = true; // allow us to proceed to check the grids col for matching potentials
                                            possibleMarkableCells.Add(new Point { row = row2, col = col }); // track this matching cell for if this find actually modifies the board
                                        }
                                    }
                                }
                            }

                            if (found1)
                            {

                                // search the grid's col for matching potentials, skip the current block
                                for (int row2 = 0; row2 < sudoku.getSize(); row2++)
                                {
                                    if (sudoku.grid.getGrid()[row2, col].blockID != sudoku.grid.getGrid()[row, col].blockID) // skip cur block
                                    {
                                        foreach (string potential2 in sudoku.grid.getGrid()[row2, col].potentialValues)
                                        {
                                            if (potential2 == potential) // if found a matching potential
                                            {
                                                found2 = true; // allow us to proceed to check the grids col for matching potentials
                                                break;
                                            }
                                        }
                                    }
                                    if (found2) break;
                                }


                                if (!found2) // if there is no other potential in the column then...
                                {
                                    List<Point> temp = new List<Point>();
                                    temp.Clear();
                                    temp.Add(new Point { row = row, col = col });
                                    foreach (Point point in possibleMarkableCells)
                                    {
                                        temp.Add(point);
                                    }
                                    markCell(temp);
                                    targetPotentials.Add(potential);
                                }
                            }
                        }
                    }
                }
            }
        }

        override
        protected bool update()
        {
            modifyingCells.Clear();
            modedCells.Clear();
            List<Point> cells = getNextMarkedCell();
            boardModified = false;

            int cellCount = -1;
            while (cells != null && sudoku.grid.getGrid()[cells[0].row, cells[0].col].potentialValues.Count > 0)
            {
                cellCount++;
                bool added = false;  // tracks if the cell has been added to the modifyingCells list or not

                List<int[]> blockLocs = sudoku.getBlockLocs();
                int startRow = blockLocs[sudoku.grid.getGrid()[cells[0].row, cells[0].col].blockID][0];
                int startCol = blockLocs[sudoku.grid.getGrid()[cells[0].row, cells[0].col].blockID][1];

                // scan each cell in cell's block
                for (int row = startRow; row < startRow + Math.Sqrt(sudoku.getSize()); row++)
                {
                    for (int col = startCol; col < startCol + Math.Sqrt(sudoku.getSize()); col++)
                    {
                        // check to make sure the cur cell is not one of the modifying cells
                        bool match = false;
                        foreach(Point cell in cells)
                        {
                            if (row == cell.row && col == cell.col)
                            {
                                match = true;
                            }
                        }

                        if (!match) // cur cell is not one of the modifying cells so proceed
                        {
                            bool removed;
                            removed = sudoku.grid.getGrid()[row, col].potentialValues.Remove(targetPotentials[cellCount]);
                            if (removed)
                            {
                                boardModified = true;
                                if (!added)
                                {
                                    foreach (Point cell2 in cells)
                                    {
                                        modifyingCells.Add(new int[2] { cell2.row, cell2.col });
                                    }
                                    added = true;
                                }
                                modedCells.Add(new int[2] { row, col });
                            }
                        }
                    }
                }

                cells = getNextMarkedCell();
            }

            return boardModified;
        }
    }
}
