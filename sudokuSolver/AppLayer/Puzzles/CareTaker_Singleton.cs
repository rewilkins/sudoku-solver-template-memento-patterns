﻿using System.Collections.Generic;

namespace AppLayer.Puzzle
{
    public class CareTaker
    {
        private volatile static CareTaker uniqueInstance;
        private readonly static object _myLock = new object();

        private Stack<Memento> mementoStack = new Stack<Memento>();
        private List<Memento> mementoList = new List<Memento>();

        private CareTaker() { }

        public static CareTaker getInstance()
        {
            if (uniqueInstance == null)
            {
                lock (_myLock)
                {
                    if (uniqueInstance == null)
                    {
                        uniqueInstance = new CareTaker();
                    }
                }
            }
            return uniqueInstance;
        }

        public void storeGridToStack(Memento state)
        {
            mementoStack.Push(state);
        }

        public Memento retrieveGridFromStack()
        {
            return mementoStack.Pop();
        }

        public int getStackCount()
        {
            return mementoStack.Count;
        }

        public void clearStack()
        {
            mementoStack.Clear();
        }


        public void storeGridToList(Memento state)
        {
            mementoList.Add(state);
        }

        public Memento retrieveGridFromList(int index)
        {
            Memento temp = mementoList[index];
            mementoList.Remove(temp);
            return temp;
        }

        public int getListCount()
        {
            return mementoList.Count;
        }

        public void clearList()
        {
            mementoList.Clear();
        }

    }
}
