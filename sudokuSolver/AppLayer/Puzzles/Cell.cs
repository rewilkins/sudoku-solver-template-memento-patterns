﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Puzzle
{
    public class Cell
    {
        public int rowID;
        public int colID;
        public int blockID;

        public string value;
        public List<string> potentialValues = new List<string>();

        public Cell() { }

        public Cell(Cell host)
        {
            rowID = host.rowID;
            colID = host.colID;
            blockID = host.blockID;

            value = (string)host.value.Clone();
            for (int i = 0; i < host.potentialValues.Count; i++)
            {
                potentialValues.Add((string)host.potentialValues[i].Clone());
            }
        }
    }
}
