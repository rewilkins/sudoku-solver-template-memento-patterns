﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer.Puzzle
{
    public class Grid
    {
        private Cell[,] grid;

        public Grid(Cell[,] state)
        {
            grid = state;
        }

        public Cell[,] getGrid()
        {
            return grid;
        }

        public void setGrid(Cell[,] newGrid)
        {
            grid = newGrid;
        }

        public Memento storeInMomento()
        {
            int size = (int)Math.Sqrt(grid.Length);
            Cell[,] copiedGrid = new Cell[size, size];
            // Deep copy by calling Clone method on each element
            for (int i = 0; i < size; i++) { 
                for (int j = 0; j < size; j++) {
                    copiedGrid[i, j] = new Cell(grid[i, j]);
                }
            }
            return new Memento(copiedGrid);
        }

        public void restoreFromMomento(Memento momento)
        {
            grid = momento.getGrid();
        }
    }
}
