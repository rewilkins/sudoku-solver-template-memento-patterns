﻿using AppLayer.Puzzle;
using System;

namespace AppLayer.Puzzle
{
    public class Memento
    {
        private Cell[,] grid;

        public Memento(Cell[,] state)
        {
            grid = state;
        }

        public Cell[,] getGrid()
        {
            return grid;
        }
    }
}
