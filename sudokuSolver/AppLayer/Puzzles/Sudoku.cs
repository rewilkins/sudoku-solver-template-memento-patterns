﻿using System;
using System.IO;
using System.Collections.Generic;

namespace AppLayer.Puzzle
{
    public class Sudoku
    {

        private string name;

        private int size;
        private string[] symbols;
        public bool puzzleSolved;
        private bool validPuzzle;
        public List<string[]> extraRows = new List<string[]>();
        public List<string[]> extraCols = new List<string[]>();
        private List<int[]> blockLocs = new List<int[]>();

        public Grid grid;

        public Sudoku()
        {
            puzzleSolved = false;
            validPuzzle = false;
        }

        public void makeSudoku(string fileName)
        {
            name = fileName;

            extraRows.Clear();
            extraCols.Clear();
            blockLocs.Clear();

            puzzleSolved = false;
            validPuzzle = true;
            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    try { size = Int32.Parse(sr.ReadLine()); }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                        validPuzzle = false;
                        return;
                    }

                    string readSymbols = sr.ReadLine();
                    readSymbols += " -";
                    symbols = readSymbols.Split(null);
                    if (symbols.Length - 1 != size)
                    {
                        validPuzzle = false;
                        return;
                    }

                    grid = new Grid(new Cell[size, size]);
                    for (int row = 0; row < size; row++)
                    {
                        string readValues = sr.ReadLine();
                        string[] temp = readValues.Split(null);
                        for (int col = 0; col < size; col++)
                        {
                            if (Array.Find(symbols, (x => x == temp[col])) == null)
                            {
                                validPuzzle = false;
                            }
                            grid.getGrid()[row, col] = new Cell();
                            grid.getGrid()[row, col].rowID = row;
                            grid.getGrid()[row, col].colID = col;
                            grid.getGrid()[row, col].blockID = (((int)(row / Math.Sqrt(size)) * (int)Math.Sqrt(size)) + (int)(col / Math.Sqrt(size)));
                            grid.getGrid()[row, col].value = temp[col];
                        }

                        string readValues2 = "";
                        for(int i = size; i < temp.Length; i++)
                        {
                            readValues2 += temp[i];
                            if (i != temp.Length - 1) readValues2 += " ";
                        }
                        string[] temp2 = readValues2.Split(null);
                        if(readValues2 != "") extraCols.Add(temp2);

                    }

                    while (!sr.EndOfStream)
                    {
                        validPuzzle = false;

                        string readValues2 = sr.ReadLine();
                        string[] temp2 = readValues2.Split(null);
                        extraRows.Add(temp2);
                    }
                    
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine("The file could not be read:");
                //Console.WriteLine(e.Message);
                validPuzzle = false;
            }

            for (int row = 0; row < size; row += (int)Math.Sqrt(size))
            {
                for (int col = 0; col < size; col += (int)Math.Sqrt(size))
                {
                    blockLocs.Add(new int[2] { row, col });
                }
            }
        }

        public string getName()
        {
            return name;
        }
        
        public int getSize()
        {
            return size;
        }
        
        public List<int[]> getBlockLocs()
        {
            return blockLocs;
        }

        public bool isPuzzleValid()
        {
            return validPuzzle;
        }

        public void setPuzzleInvalid()
        {
            validPuzzle = false;
        }

        public void setPuzzleValid()
        {
            validPuzzle = true;
        }

        public string[] getSymbols()
        {
            return symbols;
        }

    }
}
