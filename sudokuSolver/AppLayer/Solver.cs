﻿using System;
using System.Collections.Generic;
using AppLayer.Algorithms;
using AppLayer.Puzzle;

namespace AppLayer
{
    public class Solver
    {

        bool _showSteps;
        bool nicePrint;
        bool backTrackNeeded;
        List<Sudoku> puzzleBook = new List<Sudoku>();
        List<AlgorithmBase> algorithms = new List<AlgorithmBase>();

        public void run(bool showSteps)
        {
            _showSteps = showSteps;

            int code = 0;
            int algNum = 0;
            bool modified;
            for (int puzzleNum = 0; puzzleNum < puzzleBook.Count; puzzleNum++) // loop through all puzzles
            {
                backTrackNeeded = false;
                CareTaker.getInstance().clearList();
                CareTaker.getInstance().clearStack();

                if (puzzleNum == 7)
                {
                    Console.Write(""); // debug trap.  Change puzzleNum and place brake point here.
                }

                // code =  0: good
                // code = -1: original board
                // code = -2: insufficient algorithms
                // code = -3: invalid puzzle: no solution
                // code = -4: invalid puzzle: multiple solutions
                // code = -5: invalid puzzle: bad puzzle

                bool skipLoop = false;
                if (!puzzleBook[puzzleNum].isPuzzleValid())
                {
                    code = -5;
                    skipLoop = true;
                }
                else
                {
                    code = -1;
                    algNum = -1;
                    nicePrint = false;
                    if (_showSteps) printBoard(puzzleNum, algNum, code);
                    algorithms[0].sudoku = puzzleBook[puzzleNum];
                    algorithms[0].execute(); // FindAllPossibilities must only be run once per puzzle
                    code = 0;
                    algNum = 0;
                    if (_showSteps) printBoard(puzzleNum, algNum, code);
                }

                int opperationCount = 0;
                while (!skipLoop)
                {
                    opperationCount++;
                    nicePrint = false;

                    if (opperationCount == 2)
                    {
                        Console.Write(""); // debug trap.  Change opperationCount and place brake point here.
                    }

                    modified = false;
                    if(algorithms.Count == 0) algNum = 0;
                    for (int i = 1; i < algorithms.Count; i++) // apply each algorithm to current puzzle (except alg 0)
                    {

                        if (i == 3)
                        {
                            Console.Write(""); // debug trap.  Change i and place brake point here.
                        }

                        if (!modified)
                        {
                            algNum = i;
                            algorithms[i].sudoku = puzzleBook[puzzleNum];
                            modified = algorithms[i].execute();
                            if (modified) break;
                        }
                    }

                    bool skipPrint = false;
                    if (puzzleBook[puzzleNum].puzzleSolved)
                    {
                        if (CareTaker.getInstance().getStackCount() == 0)
                        {
                            CareTaker.getInstance().storeGridToList(puzzleBook[puzzleNum].grid.storeInMomento());
                            break;
                        }
                        if (CareTaker.getInstance().getStackCount() >= 1)
                        {
                            nicePrint = true;
                            if (_showSteps) printBoard(puzzleNum, algNum, code);
                            nicePrint = false;
                            backTrackNeeded = true;
                            CareTaker.getInstance().storeGridToList(puzzleBook[puzzleNum].grid.storeInMomento());
                            puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromStack());
                            puzzleBook[puzzleNum].puzzleSolved = false;
                            skipPrint = true;
                        }
                        if (CareTaker.getInstance().getListCount() > 1)
                        {
                            // Multiple Solutions
                            code = -4;
                            backTrackNeeded = false;
                            puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromList(0));
                            printBoard(puzzleNum, algNum, code);
                            break;
                        }
                    }

                    if (!modified)
                    {
                        // insufficient algorithms
                        code = -2; // if no algorithm modified the board, the result will end up being -2
                        if (_showSteps) printBoard(puzzleNum, algNum, code);
                        break;
                    }
                    
                    if (!puzzleBook[puzzleNum].isPuzzleValid())
                    {
                        // invalde puzzle
                        if (CareTaker.getInstance().getStackCount() > 0)
                        {
                            code = -3;
                            if (_showSteps) printBoard(puzzleNum, algNum, code);
                            backTrackNeeded = true;
                            puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromStack());
                            puzzleBook[puzzleNum].setPuzzleValid();
                            code = 0;
                            skipPrint = true;
                        }
                        else
                        {
                            code = -3;
                            if (_showSteps) printBoard(puzzleNum, algNum, code);
                            break;
                        }
                    }
                    if (!skipPrint)
                    {
                        if (algNum == algorithms.Count - 1) nicePrint = true;
                        if (_showSteps) printBoard(puzzleNum, algNum, code);
                        nicePrint = false;
                    }
                }
                if (algNum == algorithms.Count - 1 && code == 0) nicePrint = true;
                if (!_showSteps || skipLoop)
                {
                    if (CareTaker.getInstance().getListCount() == 1)
                    {
                        code = 0;
                        algNum = algorithms.Count - 1;
                        nicePrint = true;
                        puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromList(0));
                    }
                    if (CareTaker.getInstance().getListCount() > 1)
                    {
                        code = -4;
                        puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromList(0));
                        printBoard(puzzleNum, algNum, code);
                    }
                    if (code != -4) printBoard(puzzleNum, algNum, code);
                }
                nicePrint = false;

                if(CareTaker.getInstance().getListCount() == 1)
                {
                    code = 0;
                    algNum = algorithms.Count - 1;
                    nicePrint = true;
                    puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromList(0));
                    if (_showSteps) printBoard(puzzleNum, algNum, code);
                    nicePrint = false;
                }
                /*
                if (CareTaker.getInstance().getListCount() > 1)
                {
                    code = -4;
                    nicePrint = true;
                    puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromList(0));
                    if (_showSteps) printBoard(puzzleNum, algNum, code);
                    nicePrint = false;
                }
                */
            }
        }

        public void addSudoku(Sudoku sudoku)
        {
            //if (sudoku.isPuzzleValid()){
                puzzleBook.Add(sudoku);
            //}
            //else
            //{
            //    Console.WriteLine("Cant add invalid Sudoku puzzles.");
            //}
        }

        public void addAlgorithm(AlgorithmBase algorithm)
        {
            algorithms.Add(algorithm);
        }

        private void printBoard(int puzzleNum, int algNum, int code)
        {

            if (backTrackNeeded)
            {
                if (_showSteps) Console.WriteLine("Puzzle reverted to last saved state.\n\n");
                backTrackNeeded = false;
            }

            string fileName = "_puzzle result " + puzzleNum + ".txt";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(fileName))
            {

                Console.WriteLine("Puzzle Number: " + puzzleNum + ": " + puzzleBook[puzzleNum].getName());
                file.WriteLine("Puzzle Number: " + puzzleNum + ": " + puzzleBook[puzzleNum].getName());

                if (code != -1 && code != -2 && code != -3 && algNum != algorithms.Count - 1)
                { 
                    Console.WriteLine("algorithm " + algNum);
                }

                if (code == -2)
                {
                    Console.WriteLine("Insufficient algorithms");
                    file.WriteLine("Insufficient algorithms");
                }

                if (code == -3)
                {
                    Console.WriteLine("Invalid Puzzle: No Solution");
                    file.WriteLine("Invalid Puzzle: No Solution");
                }

                int numTimesToPrint = 1;

                if (code == -4)
                {
                    numTimesToPrint = 2;
                    Console.WriteLine("Invalid Puzzle: Multiple Solutions");
                    file.WriteLine("Invalid Puzzle: Multiple Solutions");
                }

                if (code == -5)
                {
                    Console.WriteLine("Invalid Puzzle: Bad Puzzle");
                    file.WriteLine("Invalid Puzzle: Bad Puzzle");

                    Console.Write("Valid symbols: ");
                    foreach (string symbol in puzzleBook[puzzleNum].getSymbols())
                    {
                        if (symbol != "-")
                        {
                            Console.Write(symbol + " ");
                        }
                    }
                    Console.Write("\n");

                    int numDashes = 0;
                    int puzzleSize = puzzleBook[puzzleNum].getSize();
                    for (int row = 0; row < puzzleSize; row++)
                    {
                        if (row % Math.Sqrt(puzzleSize) == 0)
                        {
                            numDashes = (puzzleSize * 7) + (puzzleSize + 1);
                            for (int i = 0; i < numDashes; i++)
                            {
                                Console.Write("-");
                                file.Write("-");
                            }
                            Console.Write("\n");
                            file.Write(Environment.NewLine);
                        }
                        for (int col = 0; col < puzzleSize; col++)
                        {

                            if (col % Math.Sqrt(puzzleSize) == 0)
                            {
                                Console.ResetColor();
                                Console.Write("|");
                                file.Write("|");
                            }

                            if (puzzleBook[puzzleNum].grid.getGrid()[row, col] != null)
                            {
                                Console.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + "\t");
                            }
                            else
                            {
                                Console.Write("null\t");
                            }

                        }
                        Console.Write("|");
                        file.Write("|");

                        // print out any extra hanging cols
                        if (puzzleBook[puzzleNum].extraCols != null && puzzleBook[puzzleNum].extraCols.Count > 0)
                        {
                            if (row < puzzleBook[puzzleNum].extraCols.Count)
                            {
                                foreach (string extra in puzzleBook[puzzleNum].extraCols[row])
                                {
                                    Console.Write(extra + "\t");
                                }
                            }
                        }

                        Console.Write("\n");
                        file.Write(Environment.NewLine);
                    }

                    numDashes = (puzzleSize * 7) + (puzzleSize + 1);
                    for (int i = 0; i < numDashes; i++)
                    {
                        Console.Write("-");
                        file.Write("-");
                    }

                    // print out any extra hanging rows
                    if (puzzleBook[puzzleNum].extraRows != null && puzzleBook[puzzleNum].extraRows.Count > 0)
                    {
                        Console.Write("\n");
                        file.Write(Environment.NewLine);
                        for (int i = 0; i < puzzleBook[puzzleNum].extraRows.Count; i++)
                        {
                            foreach (string extra in puzzleBook[puzzleNum].extraRows[i])
                            {
                                Console.Write(extra + "\t");
                            }
                            Console.Write("\n");
                        }
                    }

                    Console.Write("\n\n\n");

                }
                else
                {

                    for (int numTimesPrinted = 0; numTimesPrinted < numTimesToPrint; numTimesPrinted++)
                    {

                        int numDashes = 0;
                        int puzzleSize = puzzleBook[puzzleNum].getSize();
                        for (int row = 0; row < puzzleSize; row++)
                        {
                            if (row % Math.Sqrt(puzzleSize) == 0)
                            {
                                if (!nicePrint) numDashes = (puzzleSize * 7) + (puzzleSize + 1);
                                if (nicePrint) numDashes = (puzzleSize * 3) + ((int)Math.Sqrt(puzzleSize) + 1);
                                for (int i = 0; i < numDashes; i++)
                                {
                                    Console.Write("-");
                                    file.Write("-");
                                }
                                Console.Write("\n");
                                file.Write(Environment.NewLine);
                            }
                            for (int col = 0; col < puzzleSize; col++)
                            {

                                if (col % Math.Sqrt(puzzleSize) == 0)
                                {
                                    Console.ResetColor();
                                    Console.Write("|");
                                    file.Write("|");
                                }

                                bool modedCell = false;
                                if (algNum > -1 && code != -4)
                                {

                                    // find out if the current cell is in both modifying list and modified list.
                                    bool doublePrint = false;
                                    foreach (int[] cord1 in algorithms[algNum].modifyingCells)
                                    {
                                        if (cord1[0] == row && cord1[1] == col)
                                        {
                                            foreach (int[] cord2 in algorithms[algNum].modedCells)
                                            {
                                                if (cord2[0] == row && cord2[1] == col)
                                                {
                                                    doublePrint = true;
                                                }
                                            }
                                        }
                                    }

                                    // paint modified cells in yellow

                                    Console.ResetColor();
                                    foreach (int[] cord in algorithms[algNum].modedCells)
                                    {
                                        if (row == cord[0] && col == cord[1])
                                        {
                                            modedCell = true;
                                            Console.ForegroundColor = ConsoleColor.Yellow;
                                            foreach (string val in puzzleBook[puzzleNum].grid.getGrid()[row, col].potentialValues)
                                            {
                                                if (nicePrint) Console.Write(" ");
                                                if (nicePrint) file.Write(" ");
                                                Console.Write(val);
                                                file.Write(val);
                                            }
                                            if (!doublePrint)
                                            {
                                                if (!nicePrint) Console.Write("\t");
                                                if (!nicePrint) file.Write("\t");
                                                if (nicePrint) Console.Write(" ");
                                                if (nicePrint) file.Write(" ");
                                            }
                                            else
                                            {
                                                Console.ResetColor();
                                                Console.Write("&");
                                                file.Write("&");
                                            }
                                            Console.ResetColor();
                                            break;
                                        }
                                    }

                                    // paint board modifying cells in red

                                    Console.ResetColor();
                                    foreach (int[] cord in algorithms[algNum].modifyingCells)
                                    {
                                        if (row == cord[0] && col == cord[1])
                                        {
                                            modedCell = true;
                                            Console.ForegroundColor = ConsoleColor.Red;

                                            // if cell has no solution, then the potentials modified the board, so print them instead.
                                            if (puzzleBook[puzzleNum].grid.getGrid()[row, col].value == "-")
                                            {
                                                // paint each modifying cell's potentials if it has no solution value
                                                foreach (string val in puzzleBook[puzzleNum].grid.getGrid()[row, col].potentialValues)
                                                {
                                                    if (nicePrint) Console.Write(" ");
                                                    if (nicePrint) file.Write(" ");
                                                    Console.Write(val);
                                                    file.Write(val);
                                                }
                                                if (!nicePrint) Console.Write("\t");
                                                if (!nicePrint) file.Write("\t");
                                                if (nicePrint) Console.Write(" ");
                                                if (nicePrint) file.Write(" ");
                                            }
                                            else
                                            {
                                                // paint modifying cell's solution value because it was just solved
                                                if (nicePrint) Console.Write(" ");
                                                if (nicePrint) file.Write(" ");
                                                if (!nicePrint) Console.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + "\t");
                                                if (!nicePrint) file.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + "\t");
                                                if (nicePrint) Console.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + " ");
                                                if (nicePrint) file.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + " ");
                                            }
                                            Console.ResetColor();
                                            break;
                                        }
                                    }

                                }

                                if (!modedCell)
                                {

                                    if (puzzleBook[puzzleNum].grid.getGrid()[row, col].value != "-")
                                    {
                                        // paint already solved cells in green

                                        Console.ForegroundColor = ConsoleColor.Green;
                                        if (nicePrint) Console.Write(" ");
                                        if (nicePrint) file.Write(" ");
                                        if (!nicePrint) Console.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + "\t");
                                        if (!nicePrint) file.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + "\t");
                                        if (nicePrint) Console.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + " ");
                                        if (nicePrint) file.Write(puzzleBook[puzzleNum].grid.getGrid()[row, col].value + " ");
                                        Console.ResetColor();
                                    }
                                    else
                                    {
                                        // paint non-modified and non-solved cells in the console's default color

                                        Console.ResetColor();
                                        foreach (string val in puzzleBook[puzzleNum].grid.getGrid()[row, col].potentialValues)
                                        {
                                            if (nicePrint) Console.Write(" ");
                                            if (nicePrint) file.Write(" ");
                                            Console.Write(val);
                                            file.Write(val);
                                        }
                                        if (!nicePrint) Console.Write("\t");
                                        if (!nicePrint) file.Write("\t");
                                        if (nicePrint) Console.Write(" ");
                                        if (nicePrint) file.Write(" ");
                                    }

                                }

                            }
                            Console.ResetColor();
                            Console.Write("|\n");
                            file.Write("|" + Environment.NewLine);
                        }
                        if (!nicePrint) numDashes = (puzzleSize * 7) + (puzzleSize + 1);
                        if (nicePrint) numDashes = (puzzleSize * 3) + ((int)Math.Sqrt(puzzleSize) + 1);
                        for (int i = 0; i < numDashes; i++)
                        {
                            Console.Write("-");
                            file.Write("-");
                        }
                        Console.Write("\n\n\n");

                        if (code == -4 && CareTaker.getInstance().getListCount() > 0)
                        {
                            puzzleBook[puzzleNum].grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromList(0));
                        }
                    }
                }
            }

        }

    }
}
