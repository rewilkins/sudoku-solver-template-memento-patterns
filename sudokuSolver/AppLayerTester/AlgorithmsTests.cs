﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppLayer;
using AppLayer.Algorithms;
using AppLayer.Puzzle;

namespace AppLayerTesting
{
    [TestClass]
    public class AlgorithmsTests
    {

        [TestMethod]
        public void FindAllPossibilities()
        {

            FindAllPossibilities findAllPossibilites = new FindAllPossibilities();

            /*
                    2 - - 1
                    1 - - 4
                    - - 4 -
                    - 2 - -
            */

            Sudoku sudoku = new Sudoku();
            sudoku.makeSudoku("TestPuzzle_FindAllPossibilities.txt");

            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "-");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



            findAllPossibilites.sudoku = sudoku;
            findAllPossibilites.execute();



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "-");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues[0], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues[1], "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues[0], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues[0], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues[0], "2");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues[1], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues[0], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues[0], "1");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues[1], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues[0], "2");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues[1], "3");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues[0], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues[1], "4");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues[0], "1");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues[1], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues[0], "3");

        }

        [TestMethod]
        public void OnlyOnePossibility()
        {

            FindAllPossibilities findAllPossibilites = new FindAllPossibilities();
            OnlyOnePossibility onlyOnePossibility = new OnlyOnePossibility();

            /*
                    2 - - 1
                    1 - - 4
                    - - 4 -
                    - 2 - -
            */

            Sudoku sudoku = new Sudoku();
            sudoku.makeSudoku("TestPuzzle_OnlyOnePossibility.txt");

            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "-");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



            findAllPossibilites.sudoku = sudoku;
            findAllPossibilites.execute();

            onlyOnePossibility.sudoku = sudoku;
            onlyOnePossibility.execute();



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues[0], "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues[0], "2");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues[0], "1");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues[0], "2");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues[0], "4");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues[0], "1");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



            onlyOnePossibility.sudoku = sudoku;
            onlyOnePossibility.execute();



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "2");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);

        }

        [TestMethod]
        public void CheckForInvalidPuzzle()
        {

            FindAllPossibilities findAllPossibilites = new FindAllPossibilities();
            CheckForInvalidPuzzle checkForInvalidPuzzle = new CheckForInvalidPuzzle();

            /*
                    4 4 - 1
                    - - - 2
                    3 - - -
                    - - 4 3
            */

            Sudoku sudoku = new Sudoku();
            sudoku.makeSudoku("TestPuzzle_CheckForInvalidPuzzle.txt");

            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "2");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            findAllPossibilites.sudoku = sudoku;
            findAllPossibilites.execute();



            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);



            checkForInvalidPuzzle.sudoku = sudoku;
            checkForInvalidPuzzle.execute();



            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "*");
            Assert.AreEqual(sudoku.isPuzzleValid(), false);

        }

        [TestMethod]
        public void IsPuzzleSolved()
        {
            FindAllPossibilities findAllPossibilites = new FindAllPossibilities();
            OnlyOnePossibility onlyOnePossibility = new OnlyOnePossibility();
            IsPuzzleSovled isPuzzleSolved = new IsPuzzleSovled();

            /*
                    2 - - 1
                    1 - - 4
                    - - 4 -
                    - 2 - -
            */

            Sudoku sudoku = new Sudoku();
            sudoku.makeSudoku("TestPuzzle_IsPuzzleSolved.txt");

            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "-");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



            findAllPossibilites.sudoku = sudoku;
            findAllPossibilites.execute();

            onlyOnePossibility.sudoku = sudoku;
            onlyOnePossibility.execute();

            onlyOnePossibility.sudoku = sudoku;
            onlyOnePossibility.execute();

            isPuzzleSolved.sudoku = sudoku;
            isPuzzleSolved.execute();


            Assert.AreEqual(sudoku.puzzleSolved, true);

            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "4");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "2");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "1");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



        }

        [TestMethod]
        public void BruteForce()
        {
            FindAllPossibilities findAllPossibilites = new FindAllPossibilities();
            OnlyOnePossibility onlyOnePossibility = new OnlyOnePossibility();
            IsPuzzleSovled isPuzzleSolved = new IsPuzzleSovled();
            BruteForce bruteForce = new BruteForce();

            /*
                    4 - - 1
                    - - - -
                    3 - - -
                    - - - 3
            */

            Sudoku sudoku = new Sudoku();
            sudoku.makeSudoku("TestPuzzle_BruteForce.txt");

            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



            findAllPossibilites.sudoku = sudoku;
            findAllPossibilites.execute();

            bruteForce.sudoku = sudoku;
            bruteForce.execute();



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "2");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues[0], "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 1);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 1);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 2);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 2);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);



            sudoku.grid.restoreFromMomento(CareTaker.getInstance().retrieveGridFromStack());



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].value, "4");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].value, "1");

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].value, "3");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].value, "-");

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].value, "-");
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].value, "3");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues[0], "3");
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues[0], "2");



            Assert.AreEqual(sudoku.grid.getGrid()[0, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 1].potentialValues.Count, 1);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 2].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[0, 3].potentialValues.Count, 0);

            Assert.AreEqual(sudoku.grid.getGrid()[1, 0].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 1].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 2].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[1, 3].potentialValues.Count, 2);

            Assert.AreEqual(sudoku.grid.getGrid()[2, 0].potentialValues.Count, 0);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 1].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 2].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[2, 3].potentialValues.Count, 2);

            Assert.AreEqual(sudoku.grid.getGrid()[3, 0].potentialValues.Count, 2);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 1].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 2].potentialValues.Count, 3);
            Assert.AreEqual(sudoku.grid.getGrid()[3, 3].potentialValues.Count, 0);

        }

    }
}
