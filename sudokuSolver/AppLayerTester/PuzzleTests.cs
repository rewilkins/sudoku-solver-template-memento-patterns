﻿using System;
using System.IO;
using AppLayer.Puzzle;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace AppLayerTesting
{
    [TestClass]
    public class PuzzleTests
    {

        [TestMethod]
        public void constructorTest()
        {
            string fileName = "Puzzle-4x4-0001.txt";
            int size;
            string[] symbols;
            string[][] puzzle;
            //bool validPuzzle = true;

            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    try { size = Int32.Parse(sr.ReadLine()); }
                    catch (FormatException e)
                    {
                        Console.WriteLine(e.Message);
                        //validPuzzle = false;
                        return;
                    }
                    Assert.AreEqual(size, 4);

                    string readSymbols = sr.ReadLine();
                    readSymbols += " -";
                    symbols = readSymbols.Split(null);
                    if (symbols.Length != size)
                    {
                        //validPuzzle = false;
                        return;
                    }
                    Assert.AreEqual(symbols.Length, size);
                    for (int i = 1; i < size; i++)
                    {
                        Assert.AreEqual(symbols[i - 1], i);
                    }
                    Assert.AreEqual(symbols[size], "-");

                    puzzle = new string[size][];
                    for (int row = 0; row < size; row++)
                    {
                        string readValues = sr.ReadLine();
                        string[] temp = readValues.Split(null);
                        for (int col = 0; col < size; col++)
                        {
                            if (Array.Find(symbols, (x => x == temp[col])) == null)
                            {
                                //validPuzzle = false;
                                return;
                            }
                            puzzle[row][col] = temp[col];
                        }
                    }
                    /*
                    2 - 3 1
                    1 3 - 4
                    3 1 4 -
                    - 2 1 3
                    */
                    Assert.AreEqual(puzzle[0][0], "2");
                    Assert.AreEqual(puzzle[0][0], "-");
                    Assert.AreEqual(puzzle[0][0], "3");
                    Assert.AreEqual(puzzle[0][0], "1");

                    Assert.AreEqual(puzzle[0][0], "1");
                    Assert.AreEqual(puzzle[0][0], "3");
                    Assert.AreEqual(puzzle[0][0], "-");
                    Assert.AreEqual(puzzle[0][0], "4");

                    Assert.AreEqual(puzzle[0][0], "3");
                    Assert.AreEqual(puzzle[0][0], "1");
                    Assert.AreEqual(puzzle[0][0], "4");
                    Assert.AreEqual(puzzle[0][0], "-");

                    Assert.AreEqual(puzzle[0][0], "-");
                    Assert.AreEqual(puzzle[0][0], "2");
                    Assert.AreEqual(puzzle[0][0], "1");
                    Assert.AreEqual(puzzle[0][0], "3");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                //validPuzzle = false;
            }
        }

        [TestMethod]
        public void noFileFoundTest()
        {
            Sudoku puzzle = new Sudoku();
            puzzle.makeSudoku("InvalidPuzzle-4x4-0000.txt");
            Assert.IsFalse(puzzle.isPuzzleValid());
        }

        [TestMethod]
        public void sizeNotNumberTest()
        {
            /*
            A
            1 2 3 4
            2 - 3 1
            1 3 - 4
            3 1 4 -
            - 2 1 3
            */
            Sudoku puzzle = new Sudoku();
            puzzle.makeSudoku("InvalidPuzzle-4x4-0001.txt");
            Assert.IsFalse(puzzle.isPuzzleValid());
        }

        [TestMethod]
        public void notEnoughSymbolsTest()
        {
            /*
            4
            1 23 4
            2 - 3 1
            1 3 - 4
            3 1 4 -
            - 2 1 3
            */
            Sudoku puzzle = new Sudoku();
            puzzle.makeSudoku("InvalidPuzzle-4x4-0002.txt");
            Assert.IsFalse(puzzle.isPuzzleValid());
        }

        [TestMethod]
        public void invalidSymbolUsedTest()
        {
            /*
            4
            1 2 3 4
            2 - 30 1
            1 30 - 4
            30 1 4 -
            - 2 1 30
            */
            Sudoku puzzle = new Sudoku();
            puzzle.makeSudoku("InvalidPuzzle-4x4-0003.txt");
            Assert.IsFalse(puzzle.isPuzzleValid());
        }

        [TestMethod]
        public void setCellBlockTest()
        {
            Sudoku puzzle = new Sudoku();
            puzzle.makeSudoku("Puzzle-4x4-0001.txt");
            for (int row = 0; row < puzzle.getSize(); row++)
            {
                for (int col = 0; col < puzzle.getSize(); col++)
                {
                    puzzle.grid.getGrid()[row, col].value = ((((int)(row / Math.Sqrt(4)) * (int)Math.Sqrt(4)) + (int)(col / Math.Sqrt(4))) + 1).ToString();
                }
            }

            Assert.AreEqual("1", puzzle.grid.getGrid()[0, 0].value);
            Assert.AreEqual("1", puzzle.grid.getGrid()[0, 1].value);
            Assert.AreEqual("1", puzzle.grid.getGrid()[1, 0].value);
            Assert.AreEqual("1", puzzle.grid.getGrid()[1, 1].value);

            Assert.AreEqual("2", puzzle.grid.getGrid()[0, 2].value);
            Assert.AreEqual("2", puzzle.grid.getGrid()[0, 3].value);
            Assert.AreEqual("2", puzzle.grid.getGrid()[1, 2].value);
            Assert.AreEqual("2", puzzle.grid.getGrid()[1, 3].value);

            Assert.AreEqual("3", puzzle.grid.getGrid()[2, 0].value);
            Assert.AreEqual("3", puzzle.grid.getGrid()[2, 1].value);
            Assert.AreEqual("3", puzzle.grid.getGrid()[3, 0].value);
            Assert.AreEqual("3", puzzle.grid.getGrid()[3, 1].value);

            Assert.AreEqual("4", puzzle.grid.getGrid()[2, 2].value);
            Assert.AreEqual("4", puzzle.grid.getGrid()[2, 3].value);
            Assert.AreEqual("4", puzzle.grid.getGrid()[3, 2].value);
            Assert.AreEqual("4", puzzle.grid.getGrid()[3, 3].value);

        }

        

    }
}
