﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer;
using AppLayer.Puzzle;
using AppLayer.Algorithms;

namespace sudokuSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            Solver solver = new Solver();

            FindAllPossibilities findAllPossibilites = new FindAllPossibilities();
            solver.addAlgorithm(findAllPossibilites);

            CheckForInvalidPuzzle checkForInvalidPuzzle = new CheckForInvalidPuzzle();
            solver.addAlgorithm(checkForInvalidPuzzle);

            OnlyOnePossibility onlyOnePossibility = new OnlyOnePossibility();
            solver.addAlgorithm(onlyOnePossibility);

            NumRequiredInSpot numRequiredInSpot = new NumRequiredInSpot();
            solver.addAlgorithm(numRequiredInSpot);

            NakedTwin nakedTwin = new NakedTwin();
            solver.addAlgorithm(nakedTwin);

            SharedSubgroups sharedSubgroups = new SharedSubgroups();
            solver.addAlgorithm(sharedSubgroups);

            BruteForce bruteForce = new BruteForce();
            solver.addAlgorithm(bruteForce);

            IsPuzzleSovled isPuzzleSolved = new IsPuzzleSovled();
            solver.addAlgorithm(isPuzzleSolved);

            //------------------------------------------------------------------------------//
            //------------------------------------------------------------------------------//

            Sudoku sudoku1 = new Sudoku();
            sudoku1.makeSudoku("Puzzle-4x4-0001.txt");
            
            Sudoku sudoku2 = new Sudoku();
            sudoku2.makeSudoku("Puzzle-4x4-0002.txt");

            Sudoku sudoku3 = new Sudoku();
            sudoku3.makeSudoku("Puzzle-4x4-0101.txt");

            Sudoku sudoku4 = new Sudoku();
            sudoku4.makeSudoku("Puzzle-4x4-0201.txt");

            Sudoku sudoku5 = new Sudoku();
            sudoku5.makeSudoku("Puzzle-4x4-0901.txt");

            Sudoku sudoku6 = new Sudoku();
            sudoku6.makeSudoku("Puzzle-4x4-0902.txt");

            Sudoku sudoku7 = new Sudoku();
            sudoku7.makeSudoku("Puzzle-4x4-0903.txt");

            Sudoku sudoku8 = new Sudoku();
            sudoku8.makeSudoku("Puzzle-4x4-0904.txt");

            Sudoku sudoku9 = new Sudoku();
            sudoku9.makeSudoku("Puzzle-4x4-0905.txt");

            Sudoku sudoku10 = new Sudoku();
            sudoku10.makeSudoku("Puzzle-4x4-0906.txt");

            Sudoku sudoku11 = new Sudoku();
            sudoku11.makeSudoku("Puzzle-4x4-0907.txt");

            //------------------------------------------------------------------------------//

            Sudoku sudoku12 = new Sudoku();
            sudoku12.makeSudoku("Puzzle-9x9-0001.txt");

            Sudoku sudoku13 = new Sudoku();
            sudoku13.makeSudoku("Puzzle-9x9-0002.txt");

            Sudoku sudoku14 = new Sudoku();
            sudoku14.makeSudoku("Puzzle-9x9-0101.txt");

            Sudoku sudoku15 = new Sudoku();
            sudoku15.makeSudoku("Puzzle-9x9-0102.txt");

            Sudoku sudoku16 = new Sudoku();
            sudoku16.makeSudoku("Puzzle-9x9-0103.txt");

            Sudoku sudoku17 = new Sudoku();
            sudoku17.makeSudoku("Puzzle-9x9-0201.txt");

            Sudoku sudoku18 = new Sudoku();
            sudoku18.makeSudoku("Puzzle-9x9-0202.txt");

            Sudoku sudoku19 = new Sudoku();
            sudoku19.makeSudoku("Puzzle-9x9-0203.txt");

            Sudoku sudoku20 = new Sudoku();
            sudoku20.makeSudoku("Puzzle-9x9-0204.txt");

            Sudoku sudoku21 = new Sudoku();
            sudoku21.makeSudoku("Puzzle-9x9-0205.txt");

            Sudoku sudoku22 = new Sudoku();
            sudoku22.makeSudoku("Puzzle-9x9-0206.txt");

            Sudoku sudoku23 = new Sudoku();
            sudoku23.makeSudoku("Puzzle-9x9-0301.txt");

            Sudoku sudoku24 = new Sudoku();
            sudoku24.makeSudoku("Puzzle-9x9-0302.txt");

            Sudoku sudoku25 = new Sudoku();
            sudoku25.makeSudoku("Puzzle-9x9-0401.txt");

            //------------------------------------------------------------------------------//

            Sudoku sudoku26 = new Sudoku();
            sudoku26.makeSudoku("Puzzle-16x16-0001.txt");

            Sudoku sudoku27 = new Sudoku();
            sudoku27.makeSudoku("Puzzle-16x16-0002.txt");

            Sudoku sudoku28 = new Sudoku();
            sudoku28.makeSudoku("Puzzle-16x16-0101.txt");

            Sudoku sudoku29 = new Sudoku();
            sudoku29.makeSudoku("Puzzle-16x16-0102.txt");

            Sudoku sudoku30 = new Sudoku();
            sudoku30.makeSudoku("Puzzle-16x16-0201.txt");

            Sudoku sudoku31 = new Sudoku();
            sudoku31.makeSudoku("Puzzle-16x16-0301.txt");

            Sudoku sudoku32 = new Sudoku();
            sudoku32.makeSudoku("Puzzle-16x16-0901.txt");

            Sudoku sudoku33 = new Sudoku();
            sudoku33.makeSudoku("Puzzle-16x16-0902.txt");

            Sudoku sudoku34 = new Sudoku();
            sudoku34.makeSudoku("Puzzle-16x16-0903.txt");

            //------------------------------------------------------------------------------//

            Sudoku sudoku35 = new Sudoku();
            sudoku35.makeSudoku("Puzzle-25x25-0101.txt");

            Sudoku sudoku36 = new Sudoku();
            sudoku36.makeSudoku("Puzzle-25x25-0202.txt");

            Sudoku sudoku37 = new Sudoku();
            sudoku37.makeSudoku("Puzzle-25x25-0901.txt");

            Sudoku sudoku38 = new Sudoku();
            sudoku38.makeSudoku("Puzzle-25x25-0904.txt");

            //------------------------------------------------------------------------------//
            //------------------------------------------------------------------------------//

            solver.addSudoku(sudoku3);
            solver.addSudoku(sudoku2);
            solver.addSudoku(sudoku3);
            solver.addSudoku(sudoku4);
            solver.addSudoku(sudoku5);
            solver.addSudoku(sudoku6);
            solver.addSudoku(sudoku7);
            solver.addSudoku(sudoku8);
            solver.addSudoku(sudoku9);
            solver.addSudoku(sudoku10);
            solver.addSudoku(sudoku11);

            //------------------------------------------------------------------------------//

            solver.addSudoku(sudoku12);
            solver.addSudoku(sudoku13);
            solver.addSudoku(sudoku14);
            solver.addSudoku(sudoku15);
            solver.addSudoku(sudoku16);
            solver.addSudoku(sudoku17);
            solver.addSudoku(sudoku18);
            solver.addSudoku(sudoku19);
            solver.addSudoku(sudoku20);
            solver.addSudoku(sudoku21);
            solver.addSudoku(sudoku22);
            solver.addSudoku(sudoku23);
            solver.addSudoku(sudoku24);
            solver.addSudoku(sudoku25);

            //------------------------------------------------------------------------------//

            solver.addSudoku(sudoku26);
            solver.addSudoku(sudoku27);
            solver.addSudoku(sudoku28);
            solver.addSudoku(sudoku29);
            solver.addSudoku(sudoku30);
            solver.addSudoku(sudoku31);
            solver.addSudoku(sudoku32);
            solver.addSudoku(sudoku33);
            solver.addSudoku(sudoku34);

            //------------------------------------------------------------------------------//

            solver.addSudoku(sudoku35);
            solver.addSudoku(sudoku36);
            solver.addSudoku(sudoku37);
            solver.addSudoku(sudoku38);
            

            solver.run(false);
        }
    }
}
